var app = angular.module('liber', []);

app.controller('inicio', function ( $scope, $http ) {

	$http({
		method: 'GET',
		url :'http://www.mocky.io/v2/5cad148d2f000061003a94a9'
	})
	.then(function ( libros ) {
		$scope.libros = libros.data;
	}, function () {
		console.info('La petición falló');
	});

 	// $scope.libros = [{
		// 				"nombre": "libro1",
		// 				"editorial": "editorial1",
		// 				"autor": "autor1",
		// 				"edicion": "edicion1"
		// 			},
		// 			{
		// 				"nombre": "libro2",
		// 				"editorial": "editorial2",
		// 				"autor": "autor2",
		// 				"edicion": "edicion2"
		// 			},
		// 			{
		// 				"nombre": "libro3",
		// 				"editorial": "editorial3",
		// 				"autor": "autor3",
		// 				"edicion": "edicion3"
		// 			},
		// 			{
		// 				"nombre": "libro4",
		// 				"editorial": "editorial4",
		// 				"autor": "autor4",
		// 				"edicion": "edicion4"
		// 			}];

	$scope.editar = function ( libro ) {

		$scope.ocultarGuardar = true;
		$scope.ocultarBorrar = false;

		$scope.pos = $scope.libros.indexOf(libro);

		$scope.nombre = libro.nombre;
		$scope.editorial = libro.editorial;
		$scope.autor = libro.autor;
		$scope.edicion = libro.edicion;

		$('#exampleModal').modal('show');

	};

	$scope.agregar = function ( ) {

		$scope.ocultarBorrar = true;
		$scope.ocultarGuardar = false;

		$scope.nombre = undefined;
		$scope.editorial = undefined;
		$scope.autor = undefined;
		$scope.edicion = undefined;

		$('#exampleModal').modal('show');

	};

	$scope.guardar = function ( ) {

		if ($scope.nombre != null && $scope.editorial != null 
			&& $scope.autor != null && $scope.edicion != null) {

			$scope.libros.push({
						nombre: $scope.nombre,
						editorial: $scope.editorial,
						autor: $scope.autor,
						edicion: $scope.edicion
					});
		};

		$('#exampleModal').modal('hide');

	};

	$scope.borrar = function ( ) {

		$scope.libros.splice($scope.pos, 1);

		$('#exampleModal').modal('hide');
	};
	
});